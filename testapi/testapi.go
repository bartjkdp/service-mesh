package testapi

import (
	"crypto/x509"
	"net/http"

	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"go.uber.org/zap"
)

// TestAPI is an API that can be used for testing
type TestAPI struct {
	logger     *zap.Logger
	roots      *x509.CertPool
	tlsOptions *tlsoptions.TLSOptions
}

// NewTestAPI creates a new Test API
func NewTestAPI(logger *zap.Logger, tlsOptions tlsoptions.TLSOptions) (*TestAPI, error) {
	t := &TestAPI{
		logger:     logger,
		tlsOptions: &tlsOptions,
	}

	return t, nil
}

// ListenAndServeTLS is a blocking function that listens on provided tcp address to handle requests.
func (c *TestAPI) ListenAndServeTLS(address string) error {
	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})

	server := &http.Server{
		Addr:    address,
		Handler: serveMux,
	}

	err := server.ListenAndServeTLS(c.tlsOptions.CertFile, c.tlsOptions.KeyFile)
	if err != nil {
		c.logger.Error("error during listening", zap.Error(err))
	}

	return nil
}
