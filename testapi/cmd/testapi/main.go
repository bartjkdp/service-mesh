package main

import (
	"fmt"
	"log"

	"github.com/svent/go-flags"

	"gitlab.com/bartjkdp/service-mesh/common/logoptions"
	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"gitlab.com/bartjkdp/service-mesh/testapi"
)

var options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:7004" description:"Address for the API to be reachable on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs." required:"true"`
	logoptions.LogOptions
	tlsoptions.TLSOptions
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	// Setup new zap logger
	zapConfig := options.LogOptions.ZapConfig()
	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger.Info(fmt.Sprintf("starting testapi on %s", options.ListenAddress))

	t, _ := testapi.NewTestAPI(logger, options.TLSOptions)

	t.ListenAndServeTLS(options.ListenAddress)
}
