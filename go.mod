module gitlab.com/bartjkdp/service-mesh

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	google.golang.org/grpc v1.25.1
)
