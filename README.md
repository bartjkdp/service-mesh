# Service mesh
This is an experimental implementation of a federated intra-organizational service mesh. It exists of the following components:

- **Directory**, a central component that can be deployed both within an organization as well as between organizations. A local directory can be linked to one or more upstream directories to synchronize local state with the remote state. The directory contains information about available services as well as ACL's for local services.
- **Gateway**, a local component that exposes local services of an organization to other organizations and provides intra-organizational ACL.
- **Connect**, an (optional) proxy sidecar that provides TLS termination and a authorization of services. It is also possible to create an own implemention of the sidecar.

## Hostname
A service can connect to another service through a specially created URL. It can be structured like this:

```
{service}.{department.?}{organization}.mesh
```

## Secure Production Identity Framework (SPIFFE)
All components in the system use a unique SPIFFE identifier to identify themselves:

```
spiffe://{department.?}{organization}.mesh/services/{service}
```

## Configuration

### Directory
A directory is run at either a local or global level.

```json
{
    "cert": "./internal.crt",
    "key": "./internal.key"
}
```

### Gateway
A gateway can either be configured with a list of services or with a local directory that automatically propagates all external services. The gateway also handles the communication with upstream directories to announce new services and update the local services.

```json
{
    "external_ca": "./external-ca.crt",
    "external_cert": "./external.crt",
    "external_key": "./external.key",
    "internal_ca": "./internal-ca.crt",
    "internal_cert": "./internal.crt",
    "internal_key": "./internal.key",
    "directory": "directory.local",
    "upstream_directories": [
        { "host": "central-directory.nl", "direction": "both" }
    ],
    "services": [
        {
            "name": "example",
            "endpoints": ["10.0.0.1:443", "10.0.0.2:443"]
        }
    ],
    "txlog": "txlog.local"
}
```

The gateway sets up a mutual TLS connection to the service based on the internal certificate of the gateway. The gateway also provides an `INSECURE=1` flags that allows the services to be unsecured for testing purposes.

### Connect
Connect provides an easy-to-use sidecar proxy that can be used in conjunction with a service. It provides mTLS termination and service level authorization. It is also possible

```json
{
    "directory": "directory.local",
    "cert": "./example-service.local.crt",
    "key": "./example-service.local.key"
}
```

## Helm
Install the components using Helm with the following command:

```bash
cd config/ && PREFIX="service-mesh-" NAMESPACE="default" ./generate-certs.sh && cd ../

helm upgrade --install \
  --set-file caCert=config/ca.pem \
  --set-file connectCert=config/connect.pem \
  --set-file connectKey=config/connect-key.pem \
  --set-file directoryCert=config/directory.pem \
  --set-file directoryKey=config/directory-key.pem \
  --set-file gatewayCert=config/gateway.pem \
  --set-file gatewayKey=config/gateway-key.pem \
  --set-file testapiCert=config/testapi.pem \
  --set-file testapiKey=config/testapi-key.pem \
  service-mesh \
  ./helm
```

## Benchmarking
To run a wrk benchmark use:

```bash
kubectl run --rm -i --tty wrk --image=skandyla/wrk --restart=Never -- -t40 -c40000 -d30s -H 'Host: testapi.org.mesh:8080' http://service-mesh-connect:8080/
```
