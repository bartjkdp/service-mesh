package connect

import (
	"context"
	"fmt"
	"net/http/httputil"
	"net/url"

	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
	"go.uber.org/zap"
)

// SyncExternalServices synchronizes the services in the directory with the list of externalServices
func (c *Connect) SyncExternalServices() {
	c.logger.Info("started SyncExternalServices")

	client, err := c.GetDirectoryClient()
	if err != nil {
		c.logger.Fatal("could not get directory client", zap.Error(err))
		return
	}

	dirServices, err := client.ListServices(context.Background(), &directoryapi.ListServicesRequest{})
	if err != nil {
		c.logger.Fatal("could not retrieve list of services", zap.Error(err))
		return
	}

	result := map[string]*httputil.ReverseProxy{}

	for key, service := range dirServices.Services {
		url, err := url.Parse(service.Address)
		if err != nil {
			c.logger.Fatal("could not parse url", zap.Error(err))
			continue
		}

		proxy := httputil.NewSingleHostReverseProxy(url)
		proxy.Transport = c.GetMutualTLSTransport()
		result[key] = proxy
	}

	c.logger.Info(fmt.Sprintf("%+v", dirServices))
	c.logger.Info("finished SyncExternalServices")

	c.externalServices = result
}
