package connect

import (
	"crypto/tls"
	"crypto/x509"
	"net"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"go.uber.org/zap"
)

// Connect is a bi-directional proxy for receiving and sending requests on the mesh
type Connect struct {
	logger           *zap.Logger
	roots            *x509.CertPool
	tlsOptions       *tlsoptions.TLSOptions
	directoryAddress string
	selfAddress      string
	internalServices map[string]*httputil.ReverseProxy
	externalServices map[string]*httputil.ReverseProxy
}

// NewConnect creates a new Connect instance
func NewConnect(logger *zap.Logger, tlsOptions tlsoptions.TLSOptions, directoryAddress string, selfAddress string) (*Connect, error) {
	roots, _, err := tlsoptions.Load(tlsOptions)
	if err != nil {
		return nil, err
	}

	c := &Connect{
		logger:           logger,
		roots:            roots,
		tlsOptions:       &tlsOptions,
		directoryAddress: directoryAddress,
		selfAddress:      selfAddress,
	}

	go func() {
		for true {
			c.SyncExternalServices()
			time.Sleep(10 * time.Second)
		}
	}()

	return c, nil
}

// ListenAndServeInternal is a blocking function that listens on provided tcp address to handle requests.
func (c *Connect) ListenAndServeInternal(address string) error {
	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		c.HandleProxyRequest(w, r)
	})

	server := &http.Server{
		Addr:    address,
		Handler: serveMux,
	}

	err := server.ListenAndServe()
	if err != nil {
		c.logger.Error("error during listening", zap.Error(err))
	}

	return nil
}

// ListenAndServeExternalTLS is a blocking function that listens on provided tcp address to handle requests.
func (c *Connect) ListenAndServeExternalTLS(address string) error {
	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		c.HandleProxyRequest(w, r)
	})

	rootCerts, _ := tlsoptions.LoadRootCert(c.tlsOptions.RootCert)

	server := &http.Server{
		Addr:    address,
		Handler: serveMux,
		TLSConfig: &tls.Config{
			// only allow clients that present a cert signed by our root CA
			ClientCAs:  rootCerts,
			ClientAuth: tls.RequireAndVerifyClientCert,
		},
	}

	err := server.ListenAndServeTLS(c.tlsOptions.CertFile, c.tlsOptions.KeyFile)
	if err != nil {
		c.logger.Error("error during listening", zap.Error(err))
	}

	return nil
}

// HandleProxyRequest handles a HTTP request and proxies it to the right location
func (c *Connect) HandleProxyRequest(w http.ResponseWriter, r *http.Request) {
	host, _, _ := net.SplitHostPort(r.Host)
	hostParts := strings.Split(host, ".")

	if len(hostParts) < 2 {
		http.NotFound(w, r)
		return
	}

	proxy, found := c.internalServices[hostParts[0]+"."+hostParts[1]]
	if found {
		proxy.ServeHTTP(w, r)
		return
	}

	proxy, found = c.externalServices[hostParts[0]+"."+hostParts[1]]
	if found {
		proxy.ServeHTTP(w, r)
		return
	}

	http.NotFound(w, r)
}
