package main

import (
	"fmt"
	"log"

	"github.com/svent/go-flags"
	"go.uber.org/zap"

	"gitlab.com/bartjkdp/service-mesh/common/logoptions"
	"gitlab.com/bartjkdp/service-mesh/connect"
	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
)

var options struct {
	InternalListenAddress string `long:"internal-listen-address" env:"INTERNAL_LISTEN_ADDRESS" default:"127.0.0.1:8080" description:"Address for connect to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs." required:"true"`
	ExternalListenAddress string `long:"external-listen-address" env:"EXTERNAL_LISTEN_ADDRESS" default:":8443" description:"Address for connect to be reachable on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs." required:"true"`
	DirectoryAddress      string `long:"directory-address" env:"DIRECTORY_ADDRESS" description:"Address of the directory" required:"true"`
	ServiceName           string `long:"service-name" env:"SERVICE_NAME" description:"Name of the providing service"`
	ServiceAddress        string `long:"service-address" env:"SERVICE_ADDRESS" description:"Address of the providing service"`
	SelfAddress           string `long:"self-address" env:"SELF_ADDRESS" description:"Address for connect to be reachable on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	logoptions.LogOptions
	tlsoptions.TLSOptions
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	// Setup new zap logger
	zapConfig := options.LogOptions.ZapConfig()
	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger = logger.With(zap.String("self-address", options.SelfAddress))

	//grpc_zap.ReplaceGrpcLogger(logger)

	logger.Info(fmt.Sprintf("starting connect on %s", options.InternalListenAddress))

	c, _ := connect.NewConnect(logger, options.TLSOptions, options.DirectoryAddress, options.SelfAddress)

	if options.ServiceName != "" && options.ServiceAddress != "" {
		c.RegisterService(options.ServiceName, options.ServiceAddress)
	}

	go c.ListenAndServeInternal(options.InternalListenAddress)
	c.ListenAndServeExternalTLS(options.ExternalListenAddress)
}
