package connect

import (
	"context"
	"net/http/httputil"
	"net/url"

	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
	"go.uber.org/zap"
)

// RegisterService registers a service in the directory
func (c *Connect) RegisterService(serviceName string, serviceAddress string) {
	c.logger.Info("started RegisterService")

	client, err := c.GetDirectoryClient()
	if err != nil {
		c.logger.Fatal("could not get directory client", zap.Error(err))
		return
	}

	_, err = client.CreateService(context.Background(), &directoryapi.Service{
		Name:    serviceName,
		Address: "https://" + c.selfAddress,
	})

	if err != nil {
		c.logger.Fatal("could not create service in directory", zap.Error(err))
		return
	}

	url, err := url.Parse(serviceAddress)
	if err != nil {
		c.logger.Fatal("could not parse url", zap.Error(err))
		return
	}

	internalServices := map[string]*httputil.ReverseProxy{}

	internalServices[serviceName] = httputil.NewSingleHostReverseProxy(url)
	internalServices[serviceName].Transport = c.GetMutualTLSTransport()
	c.internalServices = internalServices

	c.logger.Info("finished RegisterService")
}
