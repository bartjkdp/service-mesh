package tlsoptions

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	"github.com/pkg/errors"
)

// TLSOptions defines the TLS options for a component.
type TLSOptions struct {
	RootCert string `long:"tls-root-cert" env:"TLS_ROOT_CERT" description:"Absolute or relative path to the root cert" required:"true"`
	CertFile string `long:"tls-cert" env:"TLS_CERT" description:"Absolute or relative path to the component cert" required:"true"`
	KeyFile  string `long:"tls-key" env:"TLS_KEY" description:"Absolute or relative path to the component key" required:"true"`
}

// Load loads the root certs and own cert/key
func Load(options TLSOptions) (*x509.CertPool, *tls.Certificate, error) {
	roots, err := LoadRootCert(options.RootCert)
	if err != nil {
		return nil, nil, err
	}

	keyPair, err := tls.LoadX509KeyPair(options.CertFile, options.KeyFile)

	return roots, &keyPair, nil
}

// LoadRootCert loads the certificate from file and adds it to a new x509.CertPool which is returned.
func LoadRootCert(rootCertFile string) (*x509.CertPool, error) {
	roots := x509.NewCertPool()
	rootPEM, err := ioutil.ReadFile(rootCertFile)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to read root CA certificate file `%s`", rootCertFile)
	}
	ok := roots.AppendCertsFromPEM(rootPEM)
	if !ok {
		return nil, errors.Errorf("failed to parse PEM for root certificate `%s`", rootCertFile)
	}
	return roots, nil
}
