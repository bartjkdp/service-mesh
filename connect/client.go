package connect

import (
	"context"
	"crypto/tls"
	"time"

	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// GetDirectoryClient returns a configured client for the directory
func (c *Connect) GetDirectoryClient() (directoryapi.DirectoryClient, error) {
	rootCerts, err := tlsoptions.LoadRootCert(c.tlsOptions.RootCert)
	if err != nil {
		return nil, err
	}

	orgKeypair, err := tls.LoadX509KeyPair(c.tlsOptions.CertFile, c.tlsOptions.KeyFile)
	if err != nil {
		return nil, err
	}

	directoryConnCtx, directoryConnCtxCancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer directoryConnCtxCancel()

	directoryDialCredentials := credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{orgKeypair},
		RootCAs:      rootCerts,
	})

	directoryDialOptions := []grpc.DialOption{
		grpc.WithTransportCredentials(directoryDialCredentials),
	}

	directoryConn, err := grpc.DialContext(directoryConnCtx, c.directoryAddress, directoryDialOptions...)
	if err != nil {
		return nil, err
	}

	return directoryapi.NewDirectoryClient(directoryConn), nil
}
