package connect

import (
	"crypto/tls"

	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"golang.org/x/net/http2"
)

// GetMutualTLSTransport returns a http.Transport that is configured for mTLS
func (c *Connect) GetMutualTLSTransport() *http2.Transport {
	rootCerts, _ := tlsoptions.LoadRootCert(c.tlsOptions.RootCert)
	orgKeypair, _ := tls.LoadX509KeyPair(c.tlsOptions.CertFile, c.tlsOptions.KeyFile)

	return &http2.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs:      rootCerts,
			Certificates: []tls.Certificate{orgKeypair},
		},
	}
}
