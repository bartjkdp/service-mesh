#!/bin/sh
NAMESPACE=${NAMESPACE:-test}

echo '{"CN": "Example org", "key": {"algo": "rsa", "size": 2048}, "names": [{"O": "Example org"}]}' > ca-csr.json

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

for certHost in "connect" "gateway" "directory" "testapi"; do
    csrFilename="${certHost}-csr.json"
    echo '{"hosts": ["'${PREFIX}${certHost}'", "'${PREFIX}${certHost}'.'${NAMESPACE}'"], "key": {"algo": "rsa", "size": 2048}, "CN": "'${certHost}'", "names": [{"O": "Org 1"}]}' > "${csrFilename}"
    cfssl gencert -ca=ca.pem -ca-key=ca-key.pem "${csrFilename}" | cfssljson -bare "${certHost}"
done
