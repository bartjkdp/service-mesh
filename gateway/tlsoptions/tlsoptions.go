package tlsoptions

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	"github.com/pkg/errors"
)

// TLSOptions defines the TLS options for the gateway.
type TLSOptions struct {
	InternalRootCert string `long:"internal-tls-root-cert" env:"INTERNAL_TLS_ROOT_CERT" description:"Absolute or relative path to the internal root cert" required:"true"`
	InternalCertFile string `long:"internal-tls-cert" env:"INTERNAL_TLS_CERT" description:"Absolute or relative path to the internal cert" required:"true"`
	InternalKeyFile  string `long:"internal-tls-key" env:"INTERNAL_TLS_KEY" description:"Absolute or relative path to the internal key" required:"true"`
	ExternalRootCert string `long:"external-tls-root-cert" env:"EXTERNAL_TLS_ROOT_CERT" description:"Absolute or relative path to the external root cert" required:"true"`
	ExternalCertFile string `long:"external-tls-cert" env:"EXTERNAL_TLS_CERT" description:"Absolute or relative path to the external cert" required:"true"`
	ExternalKeyFile  string `long:"external-tls-key" env:"EXTERNAL_TLS_KEY" description:"Absolute or relative path to the external key" required:"true"`
}

// Load loads the root certs and own cert/key
func Load(options TLSOptions) (*x509.CertPool, *tls.Certificate, error) {
	roots, err := LoadRootCert(options.InternalRootCert)
	if err != nil {
		return nil, nil, err
	}

	keyPair, err := tls.LoadX509KeyPair(options.InternalCertFile, options.InternalKeyFile)

	return roots, &keyPair, nil
}

// LoadRootCert loads the certificate from file and adds it to a new x509.CertPool which is returned.
func LoadRootCert(rootCertFile string) (*x509.CertPool, error) {
	roots := x509.NewCertPool()
	rootPEM, err := ioutil.ReadFile(rootCertFile)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to read root CA certificate file `%s`", rootCertFile)
	}
	ok := roots.AppendCertsFromPEM(rootPEM)
	if !ok {
		return nil, errors.Errorf("failed to parse PEM for root certificate `%s`", rootCertFile)
	}
	return roots, nil
}
