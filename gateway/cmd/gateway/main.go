package main

import (
	"log"

	"github.com/svent/go-flags"

	"gitlab.com/bartjkdp/service-mesh/common/logoptions"
	"gitlab.com/bartjkdp/service-mesh/gateway"
	"gitlab.com/bartjkdp/service-mesh/gateway/tlsoptions"
)

var options struct {
	ListenAddress  string `long:"listen-address" env:"LISTEN_ADDRESS" default:":8443" description:"Address for gateway to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	ServiceName    string `long:"service-name" env:"SERVICE_NAME" description:"Name of the providing service"`
	ServiceAddress string `long:"service-address" env:"SERVICE_ADDRESS" description:"Address of the providing service"`
	tlsoptions.TLSOptions
	logoptions.LogOptions
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	// Setup new zap logger
	zapConfig := options.LogOptions.ZapConfig()
	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger.Info("starting connect")

	g, _ := gateway.NewGateway(logger, options.TLSOptions)
	g.ListenAndServeTLS(options.ListenAddress)
}
