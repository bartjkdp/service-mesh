package gateway

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"

	"gitlab.com/bartjkdp/service-mesh/gateway/tlsoptions"
	"go.uber.org/zap"
)

// Gateway serves as a proxy on the perimiter of an organization
type Gateway struct {
	logger     *zap.Logger
	roots      *x509.CertPool
	tlsOptions *tlsoptions.TLSOptions
}

// NewGateway creates a new Gateway instance
func NewGateway(logger *zap.Logger, tlsOptions tlsoptions.TLSOptions) (*Gateway, error) {
	roots, _, err := tlsoptions.Load(tlsOptions)
	if err != nil {
		return nil, err
	}

	g := &Gateway{
		logger:     logger,
		roots:      roots,
		tlsOptions: &tlsOptions,
	}

	return g, nil
}

// ListenAndServeTLS is a blocking function that listens on provided tcp address to handle requests.
func (g *Gateway) ListenAndServeTLS(address string) error {
	serveMux := http.NewServeMux()

	server := &http.Server{
		Addr: address,
		TLSConfig: &tls.Config{
			// only allow clients that present a cert signed by our root CA
			ClientCAs:  g.roots,
			ClientAuth: tls.RequireAndVerifyClientCert,
		},
		Handler: serveMux,
	}

	err := server.ListenAndServeTLS(g.tlsOptions.InternalCertFile, g.tlsOptions.InternalKeyFile)
	if err != nil {
		g.logger.Error("error during listening", zap.Error(err))
	}

	return nil
}
