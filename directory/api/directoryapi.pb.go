// Code generated by protoc-gen-go. DO NOT EDIT.
// source: directoryapi.proto

package directoryapi

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Service struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Address              string   `protobuf:"bytes,2,opt,name=address,proto3" json:"address,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Service) Reset()         { *m = Service{} }
func (m *Service) String() string { return proto.CompactTextString(m) }
func (*Service) ProtoMessage()    {}
func (*Service) Descriptor() ([]byte, []int) {
	return fileDescriptor_66b714ed525c1a6d, []int{0}
}

func (m *Service) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Service.Unmarshal(m, b)
}
func (m *Service) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Service.Marshal(b, m, deterministic)
}
func (m *Service) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Service.Merge(m, src)
}
func (m *Service) XXX_Size() int {
	return xxx_messageInfo_Service.Size(m)
}
func (m *Service) XXX_DiscardUnknown() {
	xxx_messageInfo_Service.DiscardUnknown(m)
}

var xxx_messageInfo_Service proto.InternalMessageInfo

func (m *Service) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Service) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

type ListServicesRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListServicesRequest) Reset()         { *m = ListServicesRequest{} }
func (m *ListServicesRequest) String() string { return proto.CompactTextString(m) }
func (*ListServicesRequest) ProtoMessage()    {}
func (*ListServicesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_66b714ed525c1a6d, []int{1}
}

func (m *ListServicesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListServicesRequest.Unmarshal(m, b)
}
func (m *ListServicesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListServicesRequest.Marshal(b, m, deterministic)
}
func (m *ListServicesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListServicesRequest.Merge(m, src)
}
func (m *ListServicesRequest) XXX_Size() int {
	return xxx_messageInfo_ListServicesRequest.Size(m)
}
func (m *ListServicesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListServicesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListServicesRequest proto.InternalMessageInfo

type ListServicesResponse struct {
	Services             map[string]*Service `protobuf:"bytes,1,rep,name=services,proto3" json:"services,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}            `json:"-"`
	XXX_unrecognized     []byte              `json:"-"`
	XXX_sizecache        int32               `json:"-"`
}

func (m *ListServicesResponse) Reset()         { *m = ListServicesResponse{} }
func (m *ListServicesResponse) String() string { return proto.CompactTextString(m) }
func (*ListServicesResponse) ProtoMessage()    {}
func (*ListServicesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_66b714ed525c1a6d, []int{2}
}

func (m *ListServicesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListServicesResponse.Unmarshal(m, b)
}
func (m *ListServicesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListServicesResponse.Marshal(b, m, deterministic)
}
func (m *ListServicesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListServicesResponse.Merge(m, src)
}
func (m *ListServicesResponse) XXX_Size() int {
	return xxx_messageInfo_ListServicesResponse.Size(m)
}
func (m *ListServicesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListServicesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListServicesResponse proto.InternalMessageInfo

func (m *ListServicesResponse) GetServices() map[string]*Service {
	if m != nil {
		return m.Services
	}
	return nil
}

func init() {
	proto.RegisterType((*Service)(nil), "directoryapi.Service")
	proto.RegisterType((*ListServicesRequest)(nil), "directoryapi.ListServicesRequest")
	proto.RegisterType((*ListServicesResponse)(nil), "directoryapi.ListServicesResponse")
	proto.RegisterMapType((map[string]*Service)(nil), "directoryapi.ListServicesResponse.ServicesEntry")
}

func init() { proto.RegisterFile("directoryapi.proto", fileDescriptor_66b714ed525c1a6d) }

var fileDescriptor_66b714ed525c1a6d = []byte{
	// 247 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x4a, 0xc9, 0x2c, 0x4a,
	0x4d, 0x2e, 0xc9, 0x2f, 0xaa, 0x4c, 0x2c, 0xc8, 0xd4, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2,
	0x41, 0x16, 0x53, 0x32, 0xe7, 0x62, 0x0f, 0x4e, 0x2d, 0x2a, 0xcb, 0x4c, 0x4e, 0x15, 0x12, 0xe2,
	0x62, 0xc9, 0x4b, 0xcc, 0x4d, 0x95, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x0c, 0x02, 0xb3, 0x85, 0x24,
	0xb8, 0xd8, 0x13, 0x53, 0x52, 0x8a, 0x52, 0x8b, 0x8b, 0x25, 0x98, 0xc0, 0xc2, 0x30, 0xae, 0x92,
	0x28, 0x97, 0xb0, 0x4f, 0x66, 0x71, 0x09, 0x54, 0x73, 0x71, 0x50, 0x6a, 0x61, 0x69, 0x6a, 0x71,
	0x89, 0xd2, 0x0e, 0x46, 0x2e, 0x11, 0x54, 0xf1, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x21, 0x1f,
	0x2e, 0x8e, 0x62, 0xa8, 0x98, 0x04, 0xa3, 0x02, 0xb3, 0x06, 0xb7, 0x91, 0x81, 0x1e, 0x8a, 0xeb,
	0xb0, 0xe9, 0xd2, 0x83, 0x09, 0xb8, 0xe6, 0x95, 0x14, 0x55, 0x06, 0xc1, 0x4d, 0x90, 0x0a, 0xe2,
	0xe2, 0x45, 0x91, 0x12, 0x12, 0xe0, 0x62, 0xce, 0x4e, 0xad, 0x84, 0xba, 0x1d, 0xc4, 0x14, 0xd2,
	0xe6, 0x62, 0x2d, 0x4b, 0xcc, 0x29, 0x4d, 0x05, 0x3b, 0x9c, 0xdb, 0x48, 0x14, 0xd5, 0x36, 0xa8,
	0xee, 0x20, 0x88, 0x1a, 0x2b, 0x26, 0x0b, 0x46, 0xa3, 0x85, 0x8c, 0x5c, 0x9c, 0x2e, 0x30, 0x35,
	0x42, 0xa1, 0x5c, 0x3c, 0xc8, 0x2e, 0x12, 0x52, 0xc4, 0xe7, 0x5a, 0xb0, 0xdf, 0xa5, 0x94, 0x08,
	0x7b, 0x48, 0xc8, 0x96, 0x8b, 0xd7, 0xb9, 0x28, 0x35, 0xb1, 0x24, 0x15, 0x16, 0xea, 0xd8, 0xdd,
	0x25, 0x85, 0x5d, 0x38, 0x89, 0x0d, 0x1c, 0x87, 0xc6, 0x80, 0x00, 0x00, 0x00, 0xff, 0xff, 0x05,
	0x8d, 0x60, 0xb6, 0xd9, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// DirectoryClient is the client API for Directory service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DirectoryClient interface {
	ListServices(ctx context.Context, in *ListServicesRequest, opts ...grpc.CallOption) (*ListServicesResponse, error)
	CreateService(ctx context.Context, in *Service, opts ...grpc.CallOption) (*Service, error)
}

type directoryClient struct {
	cc *grpc.ClientConn
}

func NewDirectoryClient(cc *grpc.ClientConn) DirectoryClient {
	return &directoryClient{cc}
}

func (c *directoryClient) ListServices(ctx context.Context, in *ListServicesRequest, opts ...grpc.CallOption) (*ListServicesResponse, error) {
	out := new(ListServicesResponse)
	err := c.cc.Invoke(ctx, "/directoryapi.Directory/ListServices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *directoryClient) CreateService(ctx context.Context, in *Service, opts ...grpc.CallOption) (*Service, error) {
	out := new(Service)
	err := c.cc.Invoke(ctx, "/directoryapi.Directory/CreateService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DirectoryServer is the server API for Directory service.
type DirectoryServer interface {
	ListServices(context.Context, *ListServicesRequest) (*ListServicesResponse, error)
	CreateService(context.Context, *Service) (*Service, error)
}

// UnimplementedDirectoryServer can be embedded to have forward compatible implementations.
type UnimplementedDirectoryServer struct {
}

func (*UnimplementedDirectoryServer) ListServices(ctx context.Context, req *ListServicesRequest) (*ListServicesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListServices not implemented")
}
func (*UnimplementedDirectoryServer) CreateService(ctx context.Context, req *Service) (*Service, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateService not implemented")
}

func RegisterDirectoryServer(s *grpc.Server, srv DirectoryServer) {
	s.RegisterService(&_Directory_serviceDesc, srv)
}

func _Directory_ListServices_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListServicesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DirectoryServer).ListServices(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/directoryapi.Directory/ListServices",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DirectoryServer).ListServices(ctx, req.(*ListServicesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Directory_CreateService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Service)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DirectoryServer).CreateService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/directoryapi.Directory/CreateService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DirectoryServer).CreateService(ctx, req.(*Service))
	}
	return interceptor(ctx, in, info, handler)
}

var _Directory_serviceDesc = grpc.ServiceDesc{
	ServiceName: "directoryapi.Directory",
	HandlerType: (*DirectoryServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListServices",
			Handler:    _Directory_ListServices_Handler,
		},
		{
			MethodName: "CreateService",
			Handler:    _Directory_CreateService_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "directoryapi.proto",
}
