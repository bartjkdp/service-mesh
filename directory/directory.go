package directory

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"net"

	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
	"gitlab.com/bartjkdp/service-mesh/directory/store"

	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Directory holds a list of all services
type Directory struct {
	logger     *zap.Logger
	roots      *x509.CertPool
	tlsOptions *tlsoptions.TLSOptions
	store      store.Store
	directoryapi.UnimplementedDirectoryServer
}

// NewDirectory creates a new Directory instance
func NewDirectory(logger *zap.Logger, tlsOptions tlsoptions.TLSOptions) (*Directory, error) {
	roots, _, err := tlsoptions.Load(tlsOptions)
	if err != nil {
		return nil, err
	}

	d := &Directory{
		logger:     logger,
		roots:      roots,
		tlsOptions: &tlsOptions,
		store:      store.NewMemoryStore(),
	}

	return d, nil
}

// ListServices returns a list of services
func (d *Directory) ListServices(ctx context.Context, request *directoryapi.ListServicesRequest) (*directoryapi.ListServicesResponse, error) {
	return &directoryapi.ListServicesResponse{
		Services: d.store.ListServices(),
	}, nil
}

// CreateService creates a new service
func (d *Directory) CreateService(ctx context.Context, service *directoryapi.Service) (*directoryapi.Service, error) {
	err := d.store.CreateService(service)
	return service, err
}

// ListenAndServeTLS is a blocking function that listens on provided tcp address to handle requests.
func (d *Directory) ListenAndServeTLS(address string) error {
	rootCerts, err := tlsoptions.LoadRootCert(d.tlsOptions.RootCert)
	if err != nil {
		d.logger.Fatal("failed to load root cert", zap.Error(err))
	}

	certKeyPair, err := tls.LoadX509KeyPair(d.tlsOptions.CertFile, d.tlsOptions.KeyFile)
	if err != nil {
		d.logger.Fatal("failed to load x509 keypair", zap.Error(err))
	}

	serverTLSConfig := &tls.Config{
		Certificates: []tls.Certificate{certKeyPair},
		ClientCAs:    rootCerts,
		NextProtos:   []string{"h2"},
		ClientAuth:   tls.RequireAndVerifyClientCert,
	}

	opts := []grpc.ServerOption{
		grpc.Creds(credentials.NewTLS(serverTLSConfig)),
	}

	grpcServer := grpc.NewServer(opts...)
	directoryapi.RegisterDirectoryServer(grpcServer, d)
	listen, err := net.Listen("tcp", address)
	if err != nil {
		d.logger.Fatal("could not create listener", zap.Error(err))
	}

	err = grpcServer.Serve(listen)
	if err != nil {
		d.logger.Error("error during listening", zap.Error(err))
	}

	return nil
}
