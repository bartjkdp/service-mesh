package main

import (
	"fmt"
	"log"

	"github.com/svent/go-flags"

	"gitlab.com/bartjkdp/service-mesh/common/logoptions"
	"gitlab.com/bartjkdp/service-mesh/connect/tlsoptions"
	"gitlab.com/bartjkdp/service-mesh/directory"
)

var options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:":8443" description:"Address for connect to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	logoptions.LogOptions
	tlsoptions.TLSOptions
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	// Setup new zap logger
	zapConfig := options.LogOptions.ZapConfig()
	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	//grpc_zap.ReplaceGrpcLogger(logger)

	logger.Info(fmt.Sprintf("starting directory on %s", options.ListenAddress))

	d, _ := directory.NewDirectory(logger, options.TLSOptions)
	d.ListenAndServeTLS(options.ListenAddress)
}
