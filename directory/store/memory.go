package store

import (
	"sync"

	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
)

// MemoryStore is an in-memory implementation of Store
type MemoryStore struct {
	services map[string]*directoryapi.Service
	lock     sync.RWMutex
}

// NewMemoryStore creates a new MemoryStore
func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		services: map[string]*directoryapi.Service{},
		lock:     sync.RWMutex{},
	}
}

// ListServices return a list of services in the store
func (store *MemoryStore) ListServices() map[string]*directoryapi.Service {
	store.lock.RLock()
	defer store.lock.RUnlock()

	return store.services
}

// CreateService adds a new service to the store
func (store *MemoryStore) CreateService(s *directoryapi.Service) error {
	store.lock.Lock()
	defer store.lock.Unlock()

	store.services[s.Name] = s
	return nil
}

// UpdateService updates a service in the store
func (store *MemoryStore) UpdateService(name string, s *directoryapi.Service) error {
	store.lock.Lock()
	defer store.lock.Unlock()

	store.services[name] = s
	return nil
}

// DeleteService deletes a service in the store
func (store *MemoryStore) DeleteService(name string) error {
	store.lock.Lock()
	defer store.lock.Unlock()

	delete(store.services, name)
	return nil
}
