package store

import (
	directoryapi "gitlab.com/bartjkdp/service-mesh/directory/api"
)

// Store holds a list of services and functions to mutate the list
type Store interface {
	ListServices() map[string]*directoryapi.Service
	CreateService(s *directoryapi.Service) error
	UpdateService(name string, s *directoryapi.Service) error
	DeleteService(name string) error
}
